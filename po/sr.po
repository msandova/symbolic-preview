# Serbian translation for symbolic-preview.
# Copyright © 2021 symbolic-preview's COPYRIGHT HOLDER
# This file is distributed under the same license as the symbolic-preview package.
# Мирослав Николић <miroslavnikolic@rocketmail.com>, 2021–2022.
msgid ""
msgstr ""
"Project-Id-Version: symbolic-preview master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/symbolic-preview/"
"issues\n"
"POT-Creation-Date: 2022-03-12 20:53+0000\n"
"PO-Revision-Date: 2022-03-14 08:10+0200\n"
"Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>\n"
"Language-Team: Serbian <(nothing)>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: data/org.gnome.design.SymbolicPreview.metainfo.xml.in.in:7
#: data/org.gnome.design.SymbolicPreview.desktop.in.in:3
#: data/resources/ui/window.ui:44 src/widgets/window.rs:256
#: src/application.rs:169
msgid "Symbolic Preview"
msgstr "Претпреглед Симболика"

#: data/org.gnome.design.SymbolicPreview.metainfo.xml.in.in:8
#: data/resources/ui/window.ui:102 src/application.rs:172
msgid "Symbolics Made Easy"
msgstr "Лако направљени симболици"

#: data/org.gnome.design.SymbolicPreview.metainfo.xml.in.in:10
msgid ""
"Symbolic Preview is a utility that helps you create, preview and export your "
"symbolic icons easily."
msgstr ""
"Претпреглед Симболика јесте алат који вам помаже да лако направите, "
"прегледате и извезете ваше симболичке иконице."

#: data/org.gnome.design.SymbolicPreview.metainfo.xml.in.in:15
msgid "Main Window"
msgstr "Главни прозор"

#: data/org.gnome.design.SymbolicPreview.metainfo.xml.in.in:19
#| msgid "New Symbolic Icon"
msgid "Single Symbolic Icon"
msgstr "Једна симболичка иконица"

#: data/org.gnome.design.SymbolicPreview.metainfo.xml.in.in:23
#| msgid "New Icons Set"
msgid "Icons Set"
msgstr "Скуп иконица"

#: data/org.gnome.design.SymbolicPreview.metainfo.xml.in.in:63
msgid "Bilal Elmoussaoui"
msgstr "Билал Елмусауи"

#: data/org.gnome.design.SymbolicPreview.desktop.in.in:4
msgid "Symbolics made easy"
msgstr "Лако направљени симболици"

#: data/org.gnome.design.SymbolicPreview.desktop.in.in:9
msgid "Gnome;GTK;Symbolics;Icon;"
msgstr "Гном;Гтк;симболици;иконица;Gnome;GTK;Symbolics;Icon;"

#: data/org.gnome.design.SymbolicPreview.gschema.xml.in:6
#: data/org.gnome.design.SymbolicPreview.gschema.xml.in:7
msgid "Default window width"
msgstr "Основна ширина прозора"

#: data/org.gnome.design.SymbolicPreview.gschema.xml.in:11
#: data/org.gnome.design.SymbolicPreview.gschema.xml.in:12
msgid "Default window height"
msgstr "Основна висина прозора"

#: data/org.gnome.design.SymbolicPreview.gschema.xml.in:16
msgid "Default window maximized behaviour"
msgstr "Основно понашање увећаног прозора"

#: data/org.gnome.design.SymbolicPreview.gschema.xml.in:21
msgid "The 5 latest opened files seperated by a comma"
msgstr "5 последње отворених датотека раздвојених зарезом"

#: data/resources/ui/export_dialog.ui:131
msgid "Copy to Clipboard"
msgstr "Умножи у оставу"

#: data/resources/ui/export_dialog.ui:140
msgid "Save As…"
msgstr "Сачувај као…"

#: data/resources/ui/export_popover.ui:18
msgid "Export Location"
msgstr "Путања извоза"

#: data/resources/ui/export_popover.ui:36 data/resources/ui/new_project.ui:85
msgid "Browse..."
msgstr "Разгледај..."

#: data/resources/ui/export_popover.ui:49
msgid "The exported SVG file will be stored in this directory"
msgstr "Извезена СВГ датотека биће сачувана у овом директоријуму"

#: data/resources/ui/export_popover.ui:62 data/resources/ui/window.ui:63
#: src/widgets/window.rs:263
msgid "Export All"
msgstr "Извези све"

#: data/resources/ui/new_project.ui:8
#| msgid "New Symbolic Icon"
msgid "New Symbolic Icon Set"
msgstr "Нови скуп симболичких иконица"

#: data/resources/ui/new_project.ui:16 src/widgets/export/dialog.rs:48
msgid "Cancel"
msgstr "Откажи"

#: data/resources/ui/new_project.ui:22
msgid "Create"
msgstr "Направи"

#: data/resources/ui/new_project.ui:53
msgid "The source SVG file will be stored in this directory"
msgstr "Изворна СВГ датотека биће сачувана у овом директоријуму"

#: data/resources/ui/new_project.ui:69 src/widgets/new_project.rs:77
msgid "Icon Source Location"
msgstr "Место извора иконице"

#: data/resources/ui/new_project.ui:111 src/widgets/new_project.rs:78
msgid "All-lowercase, with dashes between words, e.g. list-add"
msgstr "Све малим словима, са цртицама између речи, нпр. „list-add“"

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Опште"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "New Window"
msgstr "Нови прозор"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Open File"
msgstr "Отвара датотеке"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Приказује пречице"

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Quit"
msgstr "Излази"

#: data/resources/ui/shortcuts.ui:40
msgctxt "shortcut window"
msgid "Icons Set View"
msgstr "Преглед скупа иконица"

#: data/resources/ui/shortcuts.ui:43
msgctxt "shortcut window"
msgid "Reload"
msgstr "Освежава"

#: data/resources/ui/shortcuts.ui:51
msgctxt "shortcut window"
msgid "Symbolic Icon View"
msgstr "Преглед симболичке иконице"

#: data/resources/ui/shortcuts.ui:54
msgctxt "shortcut window"
msgid "Shuffle Example icons"
msgstr "Меша иконице примера"

#: data/resources/ui/symbolic_pane.ui:323
msgid "Button"
msgstr "Дугме"

#: data/resources/ui/symbolic_pane.ui:335
msgid "Insensitive"
msgstr "Неосетљиво"

#: data/resources/ui/symbolic_pane.ui:346
msgid "Suggested"
msgstr "Предложено"

#: data/resources/ui/symbolic_pane.ui:360
msgid "Destructive"
msgstr "Рушилачко"

#: data/resources/ui/window.ui:6
msgid "New Window"
msgstr "Нови прозор"

#: data/resources/ui/window.ui:12
msgid "Reload"
msgstr "Освежи"

#: data/resources/ui/window.ui:17
msgid "Shuffle Example Icons"
msgstr "Измешај иконице примера"

#: data/resources/ui/window.ui:24
#| msgid "Keyboard Shortcuts"
msgid "_Keyboard Shortcuts"
msgstr "_Пречице тастатуре"

#: data/resources/ui/window.ui:28
#| msgid "About Symbolic Preview"
msgid "_About Symbolic Preview"
msgstr "О прегледу _Симболика"

#: data/resources/ui/window.ui:49 src/widgets/window.rs:293
msgid "_Open"
msgstr "_Отвори"

#: data/resources/ui/window.ui:51
msgid "Open an icon or an icon set"
msgstr "Отворите иконицу или скуп иконица"

#: data/resources/ui/window.ui:134
msgid "If you need an individual symbolic icon for your app"
msgstr "Ако вам је потребна посебна симболичка иконица за ваш програм"

#: data/resources/ui/window.ui:147 src/widgets/new_project.rs:75
msgid "New Symbolic Icon"
msgstr "Нова симболичка иконица"

#: data/resources/ui/window.ui:163
msgid "New Icon Set"
msgstr "Нови скуп иконица"

#: data/resources/ui/window.ui:181
msgid ""
"If your app has many custom symbolic icons, an icon set is a good way to "
"manage them"
msgstr ""
"Ако ваш програм има много произвољних симболичких иконица, скуп иконица је "
"добар начин да њима управљате"

#: src/widgets/export/dialog.rs:44
msgid "Export a symbolic icon"
msgstr "Извезите симболичку иконицу"

#: src/widgets/export/dialog.rs:47 src/widgets/window.rs:269
msgid "Export"
msgstr "Извези"

#: src/widgets/export/dialog.rs:54 src/widgets/window.rs:298
msgid "SVG images"
msgstr "СВГ слике"

#: src/widgets/export/popover.rs:85 src/widgets/new_project.rs:137
msgid "Select a Location"
msgstr "Изаберите место"

#: src/widgets/export/popover.rs:88 src/widgets/new_project.rs:140
#: src/widgets/window.rs:293
msgid "_Cancel"
msgstr "_Откажи"

#: src/widgets/export/popover.rs:88 src/widgets/new_project.rs:140
msgid "_Select"
msgstr "_Изабери"

#: src/widgets/new_project.rs:76
msgid "Icon Name"
msgstr "Назив иконице"

#: src/widgets/new_project.rs:81
msgid "New Icons Set"
msgstr "Нови скуп иконица"

#: src/widgets/new_project.rs:82
msgid "Icons Set Name"
msgstr "Назив скупа иконица"

#: src/widgets/new_project.rs:83
msgid "Icons Set Source Location"
msgstr "Место извора скупа иконица"

#: src/widgets/window.rs:290
msgid "Open File"
msgstr "Отворите датотеку"

#: src/application.rs:176
msgid "translator-credits"
msgstr ""
"Мирослав Николић <miroslavnikolic@rocketmail.com>  \n"
"\n"
"http://prevod.org — превод на српски језик"

#~ msgid "Tags:"
#~ msgstr "Ознаке:"

#~ msgid "Open"
#~ msgstr "Отвори"
