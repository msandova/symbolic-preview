# Spanish translation for symbolic-preview.
# Copyright (C) 2019 symbolic-preview's COPYRIGHT HOLDER
# This file is distributed under the same license as the symbolic-preview package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Daniel Mustieles <daniel.mustieles@gmail.com>, 2022-2024.
#
msgid ""
msgstr ""
"Project-Id-Version: symbolic-preview master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/symbolic-preview/"
"issues\n"
"POT-Creation-Date: 2023-10-10 21:11+0000\n"
"PO-Revision-Date: 2024-01-30 11:00+0100\n"
"Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>\n"
"Language-Team: Spanish - Spain <gnome-es-list@gnome.org>\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 45.3\n"

#: data/org.gnome.design.SymbolicPreview.metainfo.xml.in.in:7
#: data/org.gnome.design.SymbolicPreview.desktop.in.in:3
#: data/resources/ui/window.ui:48 src/widgets/window.rs:296
#: src/application.rs:147
msgid "Symbolic Preview"
msgstr "Vista previa de simbólicos"

#: data/org.gnome.design.SymbolicPreview.metainfo.xml.in.in:8
#: data/resources/ui/window.ui:108 src/application.rs:150
msgid "Symbolics Made Easy"
msgstr "Simbólicos fáciles"

#: data/org.gnome.design.SymbolicPreview.metainfo.xml.in.in:10
msgid ""
"Symbolic Preview is a utility that helps you create, preview and export your "
"symbolic icons easily."
msgstr ""
"Vista previa de simbólicos es una utilidad que le ayuda a crear, "
"previsualizar y exportar sus iconos fácilmente."

#: data/org.gnome.design.SymbolicPreview.metainfo.xml.in.in:15
msgid "Main Window"
msgstr "Ventana principal"

#: data/org.gnome.design.SymbolicPreview.metainfo.xml.in.in:19
msgid "Single Symbolic Icon"
msgstr "Icono simbólico único"

#: data/org.gnome.design.SymbolicPreview.metainfo.xml.in.in:83
msgid "Bilal Elmoussaoui"
msgstr "Bilal Elmoussaoui"

#: data/org.gnome.design.SymbolicPreview.desktop.in.in:4
msgid "Symbolics made easy"
msgstr "Simbólicos fáciles"

#: data/org.gnome.design.SymbolicPreview.desktop.in.in:9
msgid "Gnome;GTK;Symbolics;Icon;"
msgstr "Gnome;GTK;simbólicos;icono;"

#: data/org.gnome.design.SymbolicPreview.gschema.xml.in:6
#: data/org.gnome.design.SymbolicPreview.gschema.xml.in:7
msgid "Default window width"
msgstr "Anchura predeterminada de ventana"

#: data/org.gnome.design.SymbolicPreview.gschema.xml.in:11
#: data/org.gnome.design.SymbolicPreview.gschema.xml.in:12
msgid "Default window height"
msgstr "Altura predeterminada de ventana"

#: data/org.gnome.design.SymbolicPreview.gschema.xml.in:16
msgid "Default window maximized behavior"
msgstr "Comportamiento predeterminado de la ventana maximizada"

#: data/org.gnome.design.SymbolicPreview.gschema.xml.in:21
msgid "The 5 latest opened files seperated by a comma"
msgstr "Los 5 últimos archivos abiertos separados por una coma"

#: data/resources/ui/export_dialog.ui:129
msgid "Copy to Clipboard"
msgstr "Copiar al portapapeles"

#: data/resources/ui/export_dialog.ui:138
msgid "Save As…"
msgstr "Guardar como…"

#: data/resources/ui/export_popover.ui:18
msgid "Export Location"
msgstr "Exportar ubicación"

#: data/resources/ui/export_popover.ui:37
msgid "Browse..."
msgstr "Examinar…"

#: data/resources/ui/export_popover.ui:50
msgid "The exported SVG file will be stored in this directory"
msgstr "El archivo SVG exportado se guardará en esta carpeta"

#: data/resources/ui/export_popover.ui:63 data/resources/ui/window.ui:67
#: src/widgets/window.rs:303
msgid "Export All"
msgstr "Exportar todo"

#: data/resources/ui/new_project.ui:8
msgid "New Symbolic Icon Set"
msgstr "Nuevo conjunto de iconos simbólicos"

#: data/resources/ui/new_project.ui:16
msgid "Cancel"
msgstr "Cancelar"

#: data/resources/ui/new_project.ui:22
msgid "Create"
msgstr "Crear"

#: data/resources/ui/new_project.ui:47 src/widgets/new_project.rs:62
msgid "Icon Name"
msgstr "Nombre del icono"

#: data/resources/ui/new_project.ui:55
msgid "All-lowercase, with dashes between words, e.g. list-add"
msgstr "Todo en minúsculas, con guiones entre palabras. Ej. lista-añadir"

#: data/resources/ui/new_project.ui:71 src/widgets/new_project.rs:63
msgid "Icon Source Location"
msgstr "Ubicación de la fuente de iconos"

#: data/resources/ui/new_project.ui:72
msgid "The source SVG file will be stored in this directory"
msgstr "El archivo SVG fuente se guardará en esta carpeta"

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "General"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "New Window"
msgstr "Ventana nueva"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Open File"
msgstr "Abrir archivo"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Mostrar atajos"

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Quit"
msgstr "Salir"

#: data/resources/ui/shortcuts.ui:40
msgctxt "shortcut window"
msgid "Icons Set View"
msgstr "Vista del conjunto de iconos"

#: data/resources/ui/shortcuts.ui:43
msgctxt "shortcut window"
msgid "Reload"
msgstr "Recargar"

#: data/resources/ui/shortcuts.ui:51
msgctxt "shortcut window"
msgid "Symbolic Icon View"
msgstr "Vista de iconos simbólicos"

#: data/resources/ui/shortcuts.ui:54
msgctxt "shortcut window"
msgid "Shuffle Example icons"
msgstr "Mezclar iconos de ejemplo"

#: data/resources/ui/symbolic_pane.ui:323
msgid "Button"
msgstr "Botón"

#: data/resources/ui/symbolic_pane.ui:335
msgid "Insensitive"
msgstr "No sensible"

#: data/resources/ui/symbolic_pane.ui:346
msgid "Suggested"
msgstr "sugerido"

#: data/resources/ui/symbolic_pane.ui:360
msgid "Destructive"
msgstr "Destructivo"

#: data/resources/ui/window.ui:6
msgid "New Window"
msgstr "Ventana nueva"

#: data/resources/ui/window.ui:12
msgid "Reload"
msgstr "Recargar"

#: data/resources/ui/window.ui:17
msgid "Shuffle Example Icons"
msgstr "Mezclar iconos de ejemplo"

#: data/resources/ui/window.ui:22
msgid "Open With…"
msgstr "Abrir con…"

#: data/resources/ui/window.ui:29
msgid "_Keyboard Shortcuts"
msgstr "Atajos del _teclado"

#: data/resources/ui/window.ui:33
msgid "_About Symbolic Preview"
msgstr "_Acerca de Vista previa de simbólicos"

#: data/resources/ui/window.ui:53 src/widgets/window.rs:337
msgid "_Open"
msgstr "_Abrir"

#: data/resources/ui/window.ui:55
msgid "Open Icon or Icon Set"
msgstr "Abrir un icono o conjunto de iconos"

#: data/resources/ui/window.ui:75
msgid "Main Menu"
msgstr "Menú Principal"

#: data/resources/ui/window.ui:140
msgid "If you need an individual symbolic icon for your app"
msgstr "Si necesita un icono simbólico individual para su aplicación"

#: data/resources/ui/window.ui:153 src/widgets/new_project.rs:61
msgid "New Symbolic Icon"
msgstr "Nuevo icono simbólico"

#: data/resources/ui/window.ui:169
msgid "New Icon Set"
msgstr "Nuevo conjunto de iconos"

#: data/resources/ui/window.ui:187
msgid ""
"If your app has many custom symbolic icons, an icon set is a good way to "
"manage them"
msgstr ""
"Si su aplicación tiene muchos iconos simbólicos personalizados un conjunto "
"de iconos es una buena manera de gestionarlos"

#: src/widgets/export/dialog.rs:102 src/widgets/window.rs:331
msgid "SVG images"
msgstr "Imágenes SVG"

#: src/widgets/export/dialog.rs:110
msgid "Export a symbolic icon"
msgstr "Exportar un icono simbólico"

#: src/widgets/export/dialog.rs:111 src/widgets/window.rs:309
msgid "Export"
msgstr "Exportar"

#: src/widgets/export/popover.rs:85
msgid "Select a Location"
msgstr "Seleccionar una ubicación"

#: src/widgets/export/popover.rs:89
msgid "_Cancel"
msgstr "_Cancelar"

#: src/widgets/export/popover.rs:90
msgid "_Select"
msgstr "_Seleccionar"

#: src/widgets/new_project.rs:67
msgid "New Icons Set"
msgstr "Nuevo conjunto de iconos"

#: src/widgets/new_project.rs:68
msgid "Icons Set Name"
msgstr "Nombre del conjunto de iconos"

#: src/widgets/new_project.rs:70
msgid "Icons Set Source Location"
msgstr "Ubicación de la fuente del conjunto de iconos"

#: src/widgets/window.rs:336
msgid "Open File"
msgstr "Abrir archivo"

#: src/application.rs:154
msgid "translator-credits"
msgstr "Daniel Mustieles <daniel.mustieles@gmail.com>, 2019"

#~| msgid "New Icons Set"
#~ msgid "Icons Set"
#~ msgstr "Conjunto de iconos"

#~ msgid "Tags:"
#~ msgstr "Etiquetas:"

#~ msgid "Open"
#~ msgstr "Abrir"
