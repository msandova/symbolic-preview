use gtk::{glib, prelude::*, subclass::prelude::*};

use super::pane::{PaneStyle, SymbolicPane};
use crate::{icons::random_symbolics, project::Project};

mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct SymbolicView {
        pub(super) light_pane: SymbolicPane,
        pub(super) dark_pane: SymbolicPane,
    }

    impl Default for SymbolicView {
        fn default() -> Self {
            Self {
                light_pane: SymbolicPane::new(PaneStyle::Light),
                dark_pane: SymbolicPane::new(PaneStyle::Dark),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SymbolicView {
        const NAME: &'static str = "SymbolicView";
        type Type = super::SymbolicView;
        type ParentType = gtk::Box;
    }

    impl ObjectImpl for SymbolicView {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            obj.append(&self.light_pane);
            obj.append(&self.dark_pane);
        }
    }
    impl WidgetImpl for SymbolicView {}
    impl BoxImpl for SymbolicView {}
}

glib::wrapper! {
    pub struct SymbolicView(ObjectSubclass<imp::SymbolicView>)
        @extends gtk::Widget, gtk::Box;
}

impl SymbolicView {
    pub fn shuffle(&self) {
        let imp = self.imp();

        let samples = random_symbolics(20);
        imp.light_pane.load_samples(samples.clone());
        imp.dark_pane.load_samples(samples);
    }

    pub fn load(&self, project: &Project) {
        let imp = self.imp();
        // we only display the icon
        imp.light_pane.load_from_project(project);
        imp.dark_pane.load_from_project(project);

        self.shuffle();
    }
}
