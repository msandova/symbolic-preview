mod common;
mod pane;
mod view;

pub use common::icon_name;
pub use view::SymbolicView;
